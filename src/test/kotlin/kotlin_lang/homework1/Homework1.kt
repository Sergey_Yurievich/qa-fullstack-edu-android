package kotlin_lang.homework1

/**
 * Домашнее задание №1. Информация в README.md файле
 */
fun main(args:Array<String>) {
    val arr = arrayOf(1, 4, 3, -9, 0, 16, 29, 21, 6, 45, 9)

    val sortArray = HomeWork1.sortArray(arr)
    HomeWork1.printArray(sortArray)

    val min = HomeWork1.findMin(arr)
    println("Минимальный элемент массива = $min")

    val max = HomeWork1.findMax(arr)
    println("Максимальный элемент массива = $max")
}

class HomeWork1 {

    companion object {
        @JvmStatic
        fun sortArray(arr: Array<Int>): Array<Int> {
            var sorted = false
            while(!sorted){
                sorted = true
                for(i in 0 until arr.size-1){
                    if(arr[i] > arr[i+1]){
                        val temp = arr[i]
                        arr[i] = arr[i+1]
                        arr[i + 1] = temp
                        sorted = false
                    }
                }
            }
            return arr
        }

        fun findMin(arr: Array<Int>): Int {
            var min = arr[0]
            var x = 1
            while (x < arr.size) {
                var item = arr [x]
                min = if (min <= item) min else item
                x = x + 1
            }
            return min
        }

        fun findMax(arr: Array<Int>): Int {
            var max = arr[0]
            var x = 1
            while (x < arr.size) {
                var item = arr [x]
                max = if (max >= item) max else item
                x = x + 1
            }
            return max
        }

        fun printArray(arr: Array<Int>) {
            arr.forEach { print("$it ") }
            println()
        }
    }
}