package kotlin_lang.homework4

import kotlin.math.min

fun main(args: Array<String>) {
    val arr = arrayListOf<Int>(1, 3, -9, 16, 26, 27, 88, 5, 8)
    val cart = hashMapOf<String, Int>("potato" to 3, "tomato" to 4, "juice" to 2, "onion" to 8, "bubble-gum" to 12)

    println("Минимальный элемент коллекции = ${findMin(arr)}")
    println("Количество овощей в корзине = ${vegetableCount(cart)}")
}

fun findMin(arr: ArrayList<Int>): Int? {
    //TODO : Реализовать метод findMin()
    return arr.minOrNull()
}

fun vegetableCount(cart: HashMap<String, Int>): Int {
    val vegetables = arrayListOf<String>("potato", "tomato", "onion")
    //TODO : Реализовать метод vegetableCount()
    return vegetables.sumOf { cart.count() }
}