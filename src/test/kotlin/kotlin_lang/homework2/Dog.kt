package kotlin_lang.homework2

class Dog : Animal() {

    override fun sound() {
        println("Woof ^.^")
    }
}