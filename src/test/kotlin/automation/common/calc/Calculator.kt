package automation.common.calc

import io.qameta.allure.kotlin.Step

class Calculator {

    companion object {

        @JvmStatic
        @Step ("Функция сложения")
        fun sum(a: Int, b: Int): Int {
            return a + b
        }

        @JvmStatic
        @Step ("Функция вычитания")
        fun subtract(a: Int, b: Int): Int {
            return a - b
        }

        @JvmStatic
        @Step ("Функция умножения")
        fun multiply(a: Int, b: Int): Int {
            return a * b
        }

        @Step ("Функция деления")
        @JvmStatic
        fun divide(a: Double, b: Double): Double {
            return a / b
        }
    }
}