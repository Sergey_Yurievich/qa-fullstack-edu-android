package automation.homework1

import automation.common.calc.Calculator
import io.qameta.allure.kotlin.Allure.step
import io.qameta.allure.kotlin.Description
import io.qameta.allure.kotlin.junit4.AllureRunner
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Домашнее задание №1, №2. Информация в README.md файле
 */

@RunWith(AllureRunner :: class)
class Homework {

    @Test
    @Description("Проверка функции сложения")
    fun sum() {
        val actualResult = Calculator.sum(5, 5)
        val expectedResult = 10
        step ("Складываем переменые"){}
        step("Проверка результата") {
            assertEquals("Result incorrect", expectedResult, actualResult)
        }
    }

        @Test
        @Description("Проверка функции вычитания")
        fun subtract() {
            val actualResult = Calculator.subtract(10, 5)
            val expectedResult = 5
            step ("Вычитаем переменые"){}
            step("Проверка результата") {
                assertEquals("Result incorrect", expectedResult, actualResult)
            }
        }

        @Test
        @Description("Проверка функции умножения")
        fun multiply() {
            val actualResult = Calculator.multiply(5, 5)
            val expectedResult = 25
            step ("Умножаем переменые"){}
            step("Проверка результата") {
                assertEquals("Result incorrect", expectedResult, actualResult)
            }
        }

        @Test
        @Description("Проверка функции деления")
        fun divide() {
            val actualResult = Calculator.divide(5.0, 2.0)
            val expectedResult = 2.5
            step ("Делим переменые"){}
            step("Проверка результата") {
                assertEquals("Result incorrect", expectedResult, actualResult, 0.0)
            }
        }
    }
